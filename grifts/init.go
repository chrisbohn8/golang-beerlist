package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/chrisbohn8/sample_app/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
